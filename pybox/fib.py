def fib(limit):
    a, b = 1, 1
    yield a
    while b < limit:
        a, b = b, a+b
        yield a

#x = fib()
#print x 

#for f in fib():
#    print f

#while fib(54) < 105:
#    print 7

#for x in fib(1000):
#    if x > 210:
#        break 


#for y in fib(1001):
#    print y

x = fib(1000)
y = fib(2000)

for a in x:
    print a
    if a > 200:
        break
#for b in y:
#    print b

for b in x:
    print b 
