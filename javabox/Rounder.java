/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
public class Rounder
{
	public static void main (String[] args) throws java.lang.Exception
	{
		// your code goes here
		int n=Integer.parseInt(args[0]);
		if(n%2!=0)
			System.out.println(n);
		else 
		{
			n=n-n%10+10;
			System.out.println(n);
		}
	}
}
