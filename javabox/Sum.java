/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Sum
{
	public static void main (String[] args) throws java.lang.Exception
	{
		int n1,n2,n3;
		try{n1=Integer.parseInt(args[0]);}
		 catch (NumberFormatException nFE){
		 	System.out.println("Error");
		 	return;
		 }
		 	try{n2=Integer.parseInt(args[1]);}
		 catch (NumberFormatException nFE){
		 	System.out.println("Error");
		 	return;
		 }	try{n3=Integer.parseInt(args[2]);}
		 catch (NumberFormatException nFE){
		 	System.out.println("Error");
		 	return;
		 }
		
		if(n1<0 || n2<0 || n3<0)
			System.out.println("Error");
		n1=n1-n1%10+10;
		n2=n2-n2%10+10;
		n3=n3-n3%10+10;
		System.out.println(n1+n2+n3);

	}
}
