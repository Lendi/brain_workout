import java.io.*;
import java.util.*;
import java.lang.*;


public class ReverseOrder {

    public static void main(String[] args) {

        int max = Integer.parseInt(args[0]);
        int min = Integer.parseInt(args[1]);

        if(max < min || min < 0 || max < 0|| args[0].matches("[a-zA-z]")||args[0].matches("[a-zA-z]")){
            System.out.println("error");
        }
        else{
            for(int i = max ; i >= min ; i--){
                System.out.print(i+ " ");
            }
        }
    }

}
