/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
public class Factorial
{
	public static void main (String[] args) throws java.lang.Exception
	{
		int n=Integer.parseInt(args[0]);
		int fact=1;
		int i;
		if(n<0)
		{
			System.out.println("Error");
		}
		else if (n==0 || n==1)
		{
			System.out.println("1");			
		}
		else if(n>1)
		{
			for(i=n;i>0;i--)
			{
				fact=fact*i;
			}
			System.out.println(fact);
			return;
		}
		
	}
}
