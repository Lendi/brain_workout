import java.lang.*;
import java.io.*;
import java.util.*;

public class SignFinder {
	public static void main(String[] args) {
			int n;
		
			try{n=Integer.parseInt(args[0]);}
                	catch (NumberFormatException nFE){
                        System.out.println("Error");
                        return;
			}
		    if( n == 0) { 
				System.out.println("Zero"); 
			}
		    else if (n > 0) {
		    	System.out.println("Positive Value"); 
		    }
		    else { 
		    	System.out.println("Negative Value"); 
		    }

	}
}
