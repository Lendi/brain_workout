import java.io.*;
import java.util.*;
import java.lang.*;


public class NumbersInRange {

    public static void main(String[] args) {

        int min = Integer.parseInt(args[0]);
        int max = Integer.parseInt(args[1]);

        if(min > max || min < 0 || max < 0|| args[0].matches("[a-zA-z]")||args[0].matches("[a-zA-z]")){
            System.out.println("Error");
        }
        else{
            for(int i = min ; i <= max ; i++){
                System.out.print(i+ " ");
            }
        }
    }

}

